from bera.utils.common import (
    construct_unavailable_meteo_dict,
    construct_unavailable_neige_fraiche_dict,
    format_neige_fraiche,
    format_hist_meteo,
    merge_bera_content, merge_url_content,
)


def test_merge_bera_content():
    actual_content = 'date,massif,risque1,evolurisque1,loc1,altitude,risque2,' \
                     'evolurisque2,loc2,risque_maxi,commentaire,url_telechargement,' \
                     '00_temps,00_mer_de_nuages,00_limite_pluie_neige,' \
                     '00_isotherme_0,00_isotherme_moins_10,00_altitude_vent_1,' \
                     '00_altitude_vent_2,00_direction_vent_altitude_1,' \
                     '00_vitesse_vent_altitude_1,00_direction_vent_altitude_2,' \
                     '00_vitesse_vent_altitude_2,06_temps,06_mer_de_nuages,' \
                     '06_limite_pluie_neige,06_isotherme_0,06_isotherme_moins_10,' \
                     '06_altitude_vent_1,06_altitude_vent_2,06_direction_vent_altitude_1,' \
                     '06_vitesse_vent_altitude_1,06_direction_vent_altitude_2,' \
                     '06_vitesse_vent_altitude_2,12_temps,12_mer_de_nuages,' \
                     '12_limite_pluie_neige,12_isotherme_0,12_isotherme_moins_10,' \
                     '12_altitude_vent_1,12_altitude_vent_2,12_direction_vent_altitude_1,' \
                     '12_vitesse_vent_altitude_1,12_direction_vent_altitude_2,' \
                     '12_vitesse_vent_altitude_2,precipitation_neige_veille_altitude,' \
                     'precipitation_neige_veille_epaisseur\n2023-06-06,ANDORRE,1,,,,,,,1,' \
                     'Risque faible.,https://donneespubliques.meteofrance.fr/donnees_libres/' \
                     'Pdf/BRA/BRA.ANDORRE.20230606131351.pdf,Peu nuageux,Non,Sans objet,' \
                     '3400,5200,2000,3000,_,0,S,10,Peu nuageux,Non,Sans objet,3400,5200,' \
                     '2000,3000,_,0,W,10,Pluies éparses,Non,3000,3400,5100,2000,3000,SW,10,' \
                     'SW,10,1800,Pluie\n'

    new_content = [
        ['2023-06-07', 'ANDORRE', '1', '', '', '', '', '', '', '1', 'Risque faible.',
         'https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.ANDORRE.20230607134046.pdf',
         'Variable', 'Non', 'Sans objet', '3600', '5300', '2000', '3000',
         '_', '0', 'W', '10', 'Beau temps', 'Non', 'Sans objet', '3800',
         '5400', '2000', '3000', '_', '0', 'SW', '20', 'Brume', 'Non', 'Sans objet',
         '3800', '5500', '2000', '3000', 'SW', '10', 'SW', '20', '1800', 'Pluie']
    ]

    expected = 'date,massif,risque1,evolurisque1,loc1,altitude,risque2,evolurisque2,' \
               'loc2,risque_maxi,commentaire,url_telechargement,00_temps,' \
               '00_mer_de_nuages,00_limite_pluie_neige,00_isotherme_0,' \
               '00_isotherme_moins_10,00_altitude_vent_1,00_altitude_vent_2,' \
               '00_direction_vent_altitude_1,00_vitesse_vent_altitude_1,' \
               '00_direction_vent_altitude_2,00_vitesse_vent_altitude_2,06_temps,' \
               '06_mer_de_nuages,06_limite_pluie_neige,06_isotherme_0,' \
               '06_isotherme_moins_10,06_altitude_vent_1,06_altitude_vent_2,' \
               '06_direction_vent_altitude_1,06_vitesse_vent_altitude_1,' \
               '06_direction_vent_altitude_2,06_vitesse_vent_altitude_2,12_temps,' \
               '12_mer_de_nuages,12_limite_pluie_neige,12_isotherme_0,' \
               '12_isotherme_moins_10,12_altitude_vent_1,12_altitude_vent_2,' \
               '12_direction_vent_altitude_1,12_vitesse_vent_altitude_1,' \
               '12_direction_vent_altitude_2,12_vitesse_vent_altitude_2,' \
               'precipitation_neige_veille_altitude,' \
               'precipitation_neige_veille_epaisseur\n2023-06-07,ANDORRE,1,,,,,,,' \
               '1,Risque faible.,https://donneespubliques.meteofrance.fr/donnees_libres/' \
               'Pdf/BRA/BRA.ANDORRE.20230607134046.pdf,Variable,Non,Sans objet,3600,' \
               '5300,2000,3000,_,0,W,10,Beau temps,Non,Sans objet,3800,5400,2000,' \
               '3000,_,0,SW,20,Brume,Non,Sans objet,3800,5500,2000,3000,SW,10,SW,20,' \
               '1800,Pluie\n2023-06-06,ANDORRE,1,,,,,,,1,Risque faible.,https://' \
               'donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.ANDORRE.20230606131351.pdf,' \
               'Peu nuageux,Non,Sans objet,3400,5200,2000,3000,_,0,S,10,Peu nuageux,' \
               'Non,Sans objet,3400,5200,2000,3000,_,0,W,10,Pluies éparses,Non,' \
               '3000,3400,5100,2000,3000,SW,10,SW,10,1800,Pluie\n'

    result = merge_bera_content(actual_content, new_content)
    assert expected == result

    actual_content = 'date,massif,risque1,evolurisque1\n2023-06-06,ANDORRE,1\n'
    new_content = [
        ['2023-06-07', 'ANDORRE', '1', '', '', '', '', '', '', '1', 'Risque faible.',
         'https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.ANDORRE.20230607134046.pdf',
         'Variable', 'Non', 'Sans objet', '3600', '5300', '2000', '3000',
         '_', '0', 'W', '10', 'Beau temps', 'Non', 'Sans objet', '3800',
         '5400', '2000', '3000', '_', '0', 'SW', '20', 'Brume', 'Non', 'Sans objet',
         '3800', '5500', '2000', '3000', 'SW', '10', 'SW', '20', '1800', 'Pluie']
    ]
    expected = 'date,massif,risque1,evolurisque1,loc1,altitude,risque2,evolurisque2,' \
               'loc2,risque_maxi,commentaire,url_telechargement,00_temps,' \
               '00_mer_de_nuages,00_limite_pluie_neige,00_isotherme_0,' \
               '00_isotherme_moins_10,00_altitude_vent_1,00_altitude_vent_2,' \
               '00_direction_vent_altitude_1,00_vitesse_vent_altitude_1,' \
               '00_direction_vent_altitude_2,00_vitesse_vent_altitude_2,06_temps,' \
               '06_mer_de_nuages,06_limite_pluie_neige,06_isotherme_0,' \
               '06_isotherme_moins_10,06_altitude_vent_1,06_altitude_vent_2,' \
               '06_direction_vent_altitude_1,06_vitesse_vent_altitude_1,' \
               '06_direction_vent_altitude_2,06_vitesse_vent_altitude_2,12_temps,' \
               '12_mer_de_nuages,12_limite_pluie_neige,12_isotherme_0,' \
               '12_isotherme_moins_10,12_altitude_vent_1,12_altitude_vent_2,' \
               '12_direction_vent_altitude_1,12_vitesse_vent_altitude_1,' \
               '12_direction_vent_altitude_2,12_vitesse_vent_altitude_2,' \
               'precipitation_neige_veille_altitude,' \
               'precipitation_neige_veille_epaisseur\n2023-06-07,ANDORRE,1,,,,,,,' \
               '1,Risque faible.,https://donneespubliques.meteofrance.fr/donnees_libres/' \
               'Pdf/BRA/BRA.ANDORRE.20230607134046.pdf,Variable,Non,Sans objet,3600,' \
               '5300,2000,3000,_,0,W,10,Beau temps,Non,Sans objet,3800,5400,2000,' \
               '3000,_,0,SW,20,Brume,Non,Sans objet,3800,5500,2000,3000,SW,10,SW,20,' \
               '1800,Pluie\n2023-06-06,ANDORRE,1,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n'
    result = merge_bera_content(actual_content, new_content)
    assert expected == result


def test_merge_url_content():
    actual_content = "20230605144209\n20230606131351\n20230607134046"
    new_content = ["20230608140239"]
    expected = "20230605144209\n20230606131351\n20230607134046\n20230608140239"
    result = merge_url_content(actual_content, new_content)
    assert expected == result

    actual_content = ""
    new_content = ["20230608140239"]
    expected = "\n20230608140239"
    result = merge_url_content(actual_content, new_content)
    assert expected == result


def test_format_hist_meteo():
    unformatted_meteo = {
        "DATE": "2023-02-01T12:00:00",
        "TEMPSSENSIBLE": "1",
        "TEMPSSENSIBLEJ": "-1",
        "MERNUAGES": "-1",
        "PLUIENEIGE": "-1",
        "ISO0": "1500",
        "ISO-10": "3200",
        "DD1": "W",
        "FF1": "20",
        "DD2": "NW",
        "FF2": "40",
    }
    altitude1 = "2000"
    altitude2 = "2500"

    result = format_hist_meteo(unformatted_meteo, altitude1, altitude2)

    expected = {
        "12_temps": "Beau temps",
        "12_mer_de_nuages": "Non",
        "12_limite_pluie_neige": "Sans objet",
        "12_isotherme_0": "1500",
        "12_isotherme_moins_10": "3200",
        "12_altitude_vent_1": "2000",
        "12_altitude_vent_2": "2500",
        "12_direction_vent_altitude_1": "W",
        "12_vitesse_vent_altitude_1": "20",
        "12_direction_vent_altitude_2": "NW",
        "12_vitesse_vent_altitude_2": "40",
    }

    assert result == expected


def test_format_neige_fraiche():
    unformatted_neige_fraiche = {
        "DATE": "2023-03-21T00:00:00",
        "SS241": "0",
        "SS242": "-1",
    }
    altitude_neige_fraiche = "1800"
    result = format_neige_fraiche(unformatted_neige_fraiche, altitude_neige_fraiche)
    expected = {
        "precipitation_neige_veille_altitude": "1800",
        "precipitation_neige_veille_epaisseur": "0",
    }
    assert result == expected

    unformatted_neige_fraiche = {
        "DATE": "2023-03-21T00:00:00",
        "SS241": "-2",
        "SS242": "-1",
    }
    result = format_neige_fraiche(unformatted_neige_fraiche, altitude_neige_fraiche)
    expected = {
        "precipitation_neige_veille_altitude": "1800",
        "precipitation_neige_veille_epaisseur": "Pluie",
    }
    assert result == expected

    unformatted_neige_fraiche = {
        "DATE": "2023-03-21T00:00:00",
        "SS241": "-1",
        "SS242": "-1",
    }
    result = format_neige_fraiche(unformatted_neige_fraiche, altitude_neige_fraiche)

    expected = {
        "precipitation_neige_veille_altitude": "1800",
        "precipitation_neige_veille_epaisseur": "Absence de données",
    }
    assert result == expected


def test_construct_unavailable_meteo_dict():
    result = construct_unavailable_meteo_dict()
    expected_keys = [
        "00_temps",
        "00_mer_de_nuages",
        "00_limite_pluie_neige",
        "00_isotherme_0",
        "00_isotherme_moins_10",
        "00_altitude_vent_1",
        "00_altitude_vent_2",
        "00_direction_vent_altitude_1",
        "00_vitesse_vent_altitude_1",
        "00_direction_vent_altitude_2",
        "00_vitesse_vent_altitude_2",
        "06_temps",
        "06_mer_de_nuages",
        "06_limite_pluie_neige",
        "06_isotherme_0",
        "06_isotherme_moins_10",
        "06_altitude_vent_1",
        "06_altitude_vent_2",
        "06_direction_vent_altitude_1",
        "06_vitesse_vent_altitude_1",
        "06_direction_vent_altitude_2",
        "06_vitesse_vent_altitude_2",
        "12_temps",
        "12_mer_de_nuages",
        "12_limite_pluie_neige",
        "12_isotherme_0",
        "12_isotherme_moins_10",
        "12_altitude_vent_1",
        "12_altitude_vent_2",
        "12_direction_vent_altitude_1",
        "12_vitesse_vent_altitude_1",
        "12_direction_vent_altitude_2",
        "12_vitesse_vent_altitude_2",
    ]
    expected_values = "Absence de données"
    for key, i in zip(result.keys(), range(len(expected_keys))):
        assert key == expected_keys[i]

    for value in result.values():
        assert value == expected_values


def test_construct_unavailable_neige_fraiche_dict():
    result = construct_unavailable_neige_fraiche_dict()
    expected = {
        "precipitation_neige_veille_altitude": "Absence de données",
        "precipitation_neige_veille_epaisseur": "Absence de données",
    }
    assert result == expected
