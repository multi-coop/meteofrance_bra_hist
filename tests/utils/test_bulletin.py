import pytest
import xml.etree.ElementTree as ET

import requests
import requests_mock

from unittest.mock import patch
from mock import patch, PropertyMock
from bera.utils.bulletin import Bulletin, MassifInexistantException
from bera.utils.common import PARAMS


def test_init():
    valid_bu = Bulletin("CHARTREUSE", "20160417132702")
    assert valid_bu.jour == "20160417132702"
    assert valid_bu.massif == "CHARTREUSE"

    with pytest.raises(MassifInexistantException):
        Bulletin("CHARTREUX", "20160417132702")


def test_url():
    bu = Bulletin("CHARTREUSE", "20160417132702")
    assert (
        bu.url == "https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA"
    )


def test_path_file():
    bu = Bulletin("CHARTREUSE", "20160417132702")
    assert bu.path_file == "bera/tmp_bera.xml"


def test_jour_key():
    bu = Bulletin("CHARTREUSE", "20160417132702")

    assert len(bu.jour_key) == 10
    assert bu.jour_key[4] == "-" and bu.jour_key[7] == "-"
    assert bu.jour_key == "2016-04-17"
    assert bu.jour_key != "20160417132702"


def test_parse():
    bu = Bulletin("VERCORS", "20200517132702")
    with patch(
        "bera.utils.bulletin.Bulletin.path_file", new_callable=PropertyMock
    ) as a:
        a.return_value = "tests/xml_data/valid_bera.xml"
        risques = bu.parse_donnees_risques()
        assert len(risques) == 9
        assert "RISQUE1" in risques
        assert "EVOLURISQUE1" in risques
        assert "LOC1" in risques
        assert "ALTITUDE" in risques
        assert "RISQUE2" in risques
        assert "EVOLURISQUE2" in risques
        assert "LOC2" in risques
        assert "RISQUEMAXI" in risques
        assert "COMMENTAIRE" in risques
    with patch(
        "bera.utils.bulletin.Bulletin.path_file", new_callable=PropertyMock
    ) as a:
        a.return_value = "tests/xml_data/invalid_tag_bera.xml"
        with pytest.raises(ET.ParseError):
            bu.parse_donnees_risques()
    with patch(
        "bera.utils.bulletin.Bulletin.path_file", new_callable=PropertyMock
    ) as a:
        a.return_value = "tests/xml_data/invalid_attribute_bera.xml"
        with pytest.raises(ET.ParseError):
            bu.parse_donnees_risques()


def test_parse_donnees_meteo():
    bu = Bulletin("VERCORS", "20200517132702")
    with patch(
        "bera.utils.bulletin.Bulletin.path_file", new_callable=PropertyMock
    ) as a:
        a.return_value = "tests/xml_data/valid_bera.xml"
        meteo = bu.parse_donnees_meteo()
        assert len(meteo) == 35
        for h in ["00", "06", "12"]:
            assert f"{h}_temps" in meteo
            assert f"{h}_mer_de_nuages" in meteo
            assert f"{h}_limite_pluie_neige" in meteo
            assert f"{h}_isotherme_0" in meteo
            assert f"{h}_isotherme_moins_10" in meteo
            assert f"{h}_altitude_vent_1" in meteo
            assert f"{h}_altitude_vent_2" in meteo
            assert f"{h}_direction_vent_altitude_1" in meteo
            assert f"{h}_vitesse_vent_altitude_1" in meteo
            assert f"{h}_direction_vent_altitude_2" in meteo
            assert f"{h}_vitesse_vent_altitude_2" in meteo
        assert "precipitation_neige_veille_altitude" in meteo
        assert "precipitation_neige_veille_epaisseur" in meteo

    with patch(
        "bera.utils.bulletin.Bulletin.path_file", new_callable=PropertyMock
    ) as a:
        a.return_value = "tests/xml_data/valid_bera_unavailable_meteo_data.xml"
        meteo = bu.parse_donnees_meteo()
        for param in PARAMS[12:-2]:
            assert meteo[param] == "Absence de données"


def test_download(requests_mock):
    bu = Bulletin("VERCORS", "20200517132702")

    with open("tests/xml_data/valid_bera.xml", "r") as f_mock:
        mock_xml_content = f_mock.read()
        requests_mock.get(f"{bu.url}.{bu.massif}.{bu.jour}.xml", text=mock_xml_content)
        bu.download()

        with open(bu.path_file, "r") as f_result:
            assert f_result.read() == mock_xml_content

    # m = mocker.spy(requests, 'get')
    with patch('requests.get', side_effect=requests.exceptions.ReadTimeout()):
        with pytest.raises(requests.exceptions.ReadTimeout):
            bu.download()
        # assert m.call_count == 3


def test_append_csv():
    bu = Bulletin("VERCORS", "20200517132702")
    expected = [
        '2020-05-17',
        'VERCORS',
        'https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.VERCORS.20200517132702.pdf',
    ]
    result = bu.append_csv()
    assert expected == result

    bu.risques = {'RISQUE1': '2', 'RISQUEMAXI': '2', 'COMMENTAIRE': 'Risque limité.'}
    expected = [
        '2020-05-17',
        'VERCORS',
        '2',
        '2',
        'Risque limité.',
        'https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.VERCORS.20200517132702.pdf',
    ]
    result = bu.append_csv()
    assert expected == result

    bu.meteo = {'00_temps': 'Beau temps', '00_mer_de_nuages': 'Non'}
    expected = [
        '2020-05-17',
        'VERCORS',
        '2',
        '2',
        'Risque limité.',
        'https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.VERCORS.20200517132702.pdf',
        'Beau temps',
        'Non'
    ]
    result = bu.append_csv()
    assert expected == result
