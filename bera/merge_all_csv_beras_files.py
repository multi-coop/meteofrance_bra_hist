"""
This script aims to merge all data/<MASSIF>/hist.csv files for all moutain chains into a one single common
file data/hist.csv.
This script is executed daily in the schedulded gitlab pipeline after the automatic extraction and save of all
new beras.
This script is not supposed to be launched in a development context.
"""

import glob
import os

import pandas as pd

from bera.utils.common import init_logger

logger = init_logger()

if __name__ == '__main__':

    files_to_merge = []

    logger.info('Merge all csv files...')
    # merging the files
    files_joined = os.path.join('data/', "*/hist.csv")

    # Return a list of all joined files
    list_files = glob.glob(files_joined)

    logger.info("Merging all csv files into a single pandas dataframe ...")
    # Merge files by joining all files into the same pandas dataframe
    df = pd.concat(map(pd.read_csv, list_files), ignore_index=True)

    # Delete rows with NaN values into the date column
    df = df.dropna(subset=['date'])

    # Transform float to int
    df.risque1 = df.risque1.astype(int)
    df.evolurisque1 = df.evolurisque1.astype("Int64")
    df.altitude = df.altitude.astype("Int64")
    df.risque2 = df.risque2.astype("Int64")
    df.evolurisque2 = df.evolurisque2.astype("Int64")

    # Sort by alphabetic order for massif
    df = df.sort_values(['massif', 'date'], ascending=(True, False))

    logger.debug(f"{df}")

    # Transform the new concat dataframe into a csv file
    df.to_csv('data/hist.csv', sep=',', index=False)
    with open('data/hist.csv', 'r') as f:
        full_content = f.read()

    logger.info("Job succeeded")
