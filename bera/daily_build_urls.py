"""
This script aims to get the last datetime of BERA publication for all mountain chains, and save it into all the
data/<massif>/urls_list.txt files in the 'master' branch of the gitlab repository
https://gitlab.com/multi-coop/meteofrance_bra_hist.

This script is executed daily at 17h UTC by the gitlab workflow.

This script:
- gets the last datetime of the BERAs' publication which is normally the same datetime for all chain
  mountains ("massif" in French).

- for each chain mountain:

    - gets the file data/<massif>/urls_list.txt in the gitlab repository
      https://gitlab.com/multi-coop/meteofrance_bra_hist,

    - updates the data/<massif>/urls_list.txt file content by adding the new datetime of the last BERA publication at
      the end of the actual content,

    - save updated file data/<massif>/hist.csv locally.

This script could be launched in a development context, independently of the gitlab workflow, but it should be executed
after 16h (time of BERAs publication) to get the new BERA datetime publication.
Cf README.md in section 'Developpement' to launch this script in a development context.
"""
import base64
import os
from datetime import date

from bera.utils.common import init_logger, MASSIFS, merge_url_content
from bera.utils.extract import extract_url_dl
from bera.utils.gitlab_utils import init_project_gitlab

logger = init_logger()

if __name__ == '__main__':

    branch = os.getenv('GIT_BRANCH_NAME')
    if not branch:
        raise Exception('Unknown environment variable GIT_BRANCH_NAME - Stopping here  ...')

    logger.info('Get distant gitlab project ...')
    project = init_project_gitlab()

    logger.info('Starting the extraction of urls...')
    new_urls = extract_url_dl(no_browser=True, start_date=date.today(), end_date=date.today())

    for massif in MASSIFS:
        file_path = f'data/{massif}/urls_list.txt'
        logger.info(f'Exporting the URL to gitlab for massif : {massif}   ...')

        logger.debug('Get remote file to update')
        file = project.files.get(file_path=file_path, ref=branch)
        actual_content = base64.b64decode(file.content).decode("utf-8")

        logger.debug('Update file content')
        file.content = merge_url_content(actual_content, new_urls[massif])

        # Save file locally
        with open(file_path, 'w') as f:
            f.write(file.content)
        logger.debug(f'Successfully writing the URLs to urls_list.txt file for massif: {massif} ...')

    logger.info('Job succeeded  ...')
