"""
This script aims to get all the datetime of BERAs' publication for a given period for all mountain chains, and to save
them into all the data/<massif>/urls_list.txt files in the 'master' branch of the gitlab repository
https://gitlab.com/multi-coop/meteofrance_bra_hist.

This script was useful to get all historical datetime of all BERAs' publications anterior at the beginning of this
project.
For now, normally, all datetimes of all BERAs' publications for all mountain chains had been saved in
data/<massif>/urls_list.txt files and this script is not useful anymore.

This script does quite the same as the script daily_build_urls.py but for each day of the given period.
This script:
- gets all the datetimes of the BERAs' publication for each day of the given period ,

- for each chain mountain:

    - gets the file data/<massif>/urls_list.txt in the gitlab repository
      https://gitlab.com/multi-coop/meteofrance_bra_hist,

    - updates the data/<massif>/urls_list.txt file content by adding all the datetime of the BERA publications
      corresponding to each day of the given period in the actual content,

    - saves locally th new data/<massif>/urls_list.txt file.
"""
import base64
import logging
import os
import time
from datetime import datetime

from utils.common import init_logger, MASSIFS, merge_url_content
from utils.extract import extract_url_dl
from utils.gitlab_utils import init_project_gitlab

logger = init_logger(logging.DEBUG)

if __name__ == '__main__':
    start_time = time.time()
    branch = os.getenv('GIT_BRANCH_NAME')
    if not branch:
        raise Exception(
            'Unknown environment variable GIT_BRANCH_NAME - Stopping here  ...')

    logger.info('Get distant gitlab project ...')
    project = init_project_gitlab()

    logger.info(f'{time.time() - start_time} seconds - Starting the extraction of urls ...')
    new_urls = extract_url_dl(no_browser=True,
                              start_date=datetime(2023, 4, 29),
                              end_date=datetime(2023, 5, 24))

    for massif in MASSIFS:
        file_path = f'data/{massif}/urls_list.txt'
        logger.debug(f'{time.time() - start_time} seconds  - Exporting the URL for massif : {massif}   ...')

        logger.debug('Get remote file to update')
        file = project.files.get(file_path=file_path, ref=branch)
        actual_content = base64.b64decode(file.content).decode("utf-8")

        # Update file
        logger.debug('Update file content')
        file.content = merge_url_content(actual_content, new_urls[massif])

        # Save file locally
        with open(file_path, 'w') as f:
            f.write(file.content)
        logger.debug(f'{time.time() - start_time} seconds - Successfully writing the URLs to urls_list.txt file '
                     f'for massif: {massif}   ...')

    logger.info(f'{time.time() - start_time} seconds - Job succeeded  ...')
