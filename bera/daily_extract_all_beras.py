"""
This script aims to add new BERA information from the last BERA published for each mountain chain, and save it into all
the data/<massif>/hist.csv files in the 'master' branch of the gitlab repository
https://gitlab.com/multi-coop/meteofrance_bra_hist.

This script is executed daily after the execution of the script daily_build_url.py by the gitlab pipeline defined in
the .gitlab-ci.yml file of this project.

This scripts:
- for each chain mountain:

    - reads the file data/<massif>/urls_list.txt in the gitlab repository
      https://gitlab.com/multi-coop/meteofrance_bra_hist to get the last datetime written in it. This datetime
      corresponds to the last datetime of BERA publication for this chain mountain,

    - downloads the last BERA available in xml format thanks to the last datetime of BERA publication get previously,

    - parses the xml content into a list of datas (as date, chain mountain, avalanche risk, evolution of the risk with
    altitude, eventuals comments, url to download the BERA in pdf format, weather and snow precipitation historic
    data), for example:
      ['2023-03-29', 'CHABLAIS', '2', '', '<2500', '2500', '3', '', '>2500', '3',
        'Au-dessus de 2500 m : Risque marqué. En-dessous : Risque limité.',
        'https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.CHABLAIS.20230329140534.pdf',
        'Pluies éparses', 'Non', '2700', '3000', '4600' ','2000 ',' '2500', 'SW', '20', 'SW', '30', 'Peu nuageux,
        'Non', 'Sans objet', '3000', '4600', '2000', '2500', 'SW', '20', 'SW', '30', 'Peu nuageux', 'Non',
        'Sans objet', '3100', '4700', '2000', '2500', 'SW', '20', 'SW', '20', '1800', 'Pluie'
       ]

    - gets the file data/<massif>/hist.csv in the gitlab repository https://gitlab.com/multi-coop/meteofrance_bra_hist,

    - updates the data/<massif>/hist.csv file content by adding this list of new datas at the beginning of the
      actual content,

    - save updated file data/<massif>/hist.csv locally.

This script could be launched in a development context, independently of the gitlab pipeline, but it should be executed
after the execution of the script daily_build_urls.py to get the new BERA datetime publication.
Cf README.md in section 'Developpement' to launch this script in a development context.
"""
import base64
import os
import subprocess

from bera.utils.bulletin import Bulletin
from bera.utils.common import init_logger, MASSIFS, merge_bera_content
from bera.utils.gitlab_utils import init_project_gitlab

logger = init_logger()
if __name__ == '__main__':
    branch = os.getenv('GIT_BRANCH_NAME')
    if not branch:
        raise Exception('Unknown environment variable GIT_BRANCH_NAME - Stopping here  ...')

    logger.info('Get distant gitlab project ...')
    project = init_project_gitlab()

    logger.info('Starting the daily extract...')

    for massif in MASSIFS:
        # Lecture de la date de publication de notre fichier
        jour = subprocess.run(["tail", "-n", "1", f"data/{massif}/urls_list.txt"],
                              capture_output=True).stdout.decode('utf-8')

        # Traitement du fichier
        bulletin = Bulletin(massif, jour)
        bulletin.download()
        bulletin.parse_donnees_risques()
        bulletin.parse_donnees_meteo()
        new_data = bulletin.append_csv()

        file_path = f'data/{massif}/hist.csv'
        logger.info(f'Exporting the BERA for massif : {massif}   ...')

        logger.debug('Get remote file to update')
        file = project.files.get(file_path=file_path, ref=branch)
        actual_content = base64.b64decode(file.content).decode("utf-8")

        # Update and add files to commit
        logger.debug('Update file content')
        file.content = merge_bera_content(actual_content, [new_data])

        # Save file locally
        logger.debug('Save file locally')
        with open(file_path, 'w') as f:
            f.write(file.content)
        logger.debug(f'Successfully updated the hist.csv data file for massif: {massif}   ...')

    logger.info('Job succeeded  ...')
