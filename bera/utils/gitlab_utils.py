import logging
import os
import gitlab
from gitlab.v4.objects import Project

logger = logging.getLogger(__name__)


def init_project_gitlab() -> Project:
    """
    Get the remote gitlab project repository https://gitlab.com/multi-coop/meteofrance_bra_hist
    Returns
    -------
    gitlab Project object
    """
    if not os.getenv('CI_JOB_TOKEN') is None:
        # job token authentication used in CI
        gl = gitlab.Gitlab('https://gitlab.com', job_token=os.getenv('CI_JOB_TOKEN'))
    else:
        # personal token authentication
        token = os.getenv('GITLAB_TOKEN')
        gl = gitlab.Gitlab('https://gitlab.com', private_token=token)

    project = gl.projects.get('multi-coop/meteofrance_bra_hist')
    return project
