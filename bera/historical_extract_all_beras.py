"""
This script aims to extract information from BERAs and save these datas into hist.csv files for all mountain chain,
for a given period, such as:
- risk and its evolution depending on the altitude
- weather measures (weather, isotherms, wind, snow precipitation)
- url of downloading the BERA in pdf format.

This script was useful to get historical datas from old BERAs published before the beginning of this project.

Now this script could be useful if some other information present in the BERAs (like weather, snow
precipitations, ...) are added to the data we want to save in this project, to get all historical new data.

This script:

- for each chain mountain:

    - compiles all the datetimes contained in the file data/<massif>/urls_list.txt in a list,

    - gets the actual content of the file data/<massif>/hist.csv in the gitlab repository
      https://gitlab.com/multi-coop/meteofrance_bra_hist,

    - for each datetime (since the first BERAs published are available in xml format, the 17/12/2018):

        - downloads the last BERA available in xml format,

        - parses the xml content into a list of datas (as date, chain mountain, avalanche risk, evolution of the risk
            with altitude, eventuals comments, url to download the BERA in pdf format, meteo), for example
            ['2023-03-29', 'CHABLAIS', '2', '', '<2500', '2500', '3', '', '>2500', '3',
            'Au-dessus de 2500 m : Risque marqué. En-dessous : Risque limité.',
            'https://donneespubliques.meteofrance.fr/donnees_libres/Pdf/BRA/BRA.CHABLAIS.20230329140534.pdf',
            'Pluies éparses', 'Non', '2700', '3000', '4600' ','2000 ',' '2500', 'SW', '20', 'SW', '30', 'Peu nuageux,
            'Non', 'Sans objet', '3000', '4600', '2000', '2500', 'SW', '20', 'SW', '30', 'Peu nuageux', 'Non',
            'Sans objet', '3100', '4700', '2000', '2500', 'SW', '20', 'SW', '20', '1800', 'Pluie'
            ],

    - updates the data/<massif>/hist.csv file content by adding this list of new datas at the beginning of the
          actual content,

    - saves locally the data/<massif>/hist.csv file content updated with new contents.
"""

import base64
import logging
import os
import subprocess
import time

from utils.bulletin import Bulletin
from utils.common import init_logger, MASSIFS, merge_bera_content
from utils.gitlab_utils import init_project_gitlab

logger = init_logger(logging.DEBUG)
start_time = time.time()

branch = os.getenv('GIT_BRANCH_NAME')
if not branch:
    raise Exception(
        'Unknown environment variable GIT_BRANCH_NAME - Stopping here  ...')

logger.info('Get distant gitlab project ...')
project = init_project_gitlab()

for massif in MASSIFS:
    # Lecture de la date de publication de notre fichier
    dates_ = subprocess.run(["cat", f"data/{massif}/urls_list.txt"],
                            capture_output=True).stdout.decode('utf-8').split('\n')

    logger.debug(f"{time.time() - start_time} seconds  - Add missing data into hist.csv data file "
                 f"for massif {massif} ...")
    file_path = f'data/{massif}/hist.csv'

    # Get the actual content of hist.csv file for this massif
    logger.debug(f'{time.time() - start_time} seconds - Get remote file to update ...')
    file = project.files.get(file_path=file_path, ref=branch)
    actual_content = base64.b64decode(file.content).decode("utf-8")

    new_data = []

    for date_ in dates_:
        if int(date_) >= 20230501000000:  # Datetime of the first xml files for the bera = 20181217143136
            # Traitement du fichier
            try:
                logger.debug(f'{time.time() - start_time} seconds - Exporting bera for massif {massif} at '
                             f'this date {date_}...')
                bulletin = Bulletin(massif, date_)
                bulletin.download()
                bulletin.parse_donnees_risques()
                bulletin.parse_donnees_meteo()
                new_data.append(bulletin.append_csv())
            except Exception as e:
                logger.error(
                    f'{time.time() - start_time} seconds - Error occurred for massif {massif} '
                    f'at this date {date_}: {e} ...', exc_info=True)

    # Update file
    logger.debug(f'{time.time() - start_time} seconds - Update file content')
    file.content = merge_bera_content(actual_content, new_data)

    # Save file locally
    logger.debug('Save file locally')
    with open(file_path, 'w') as f:
        f.write(file.content)
    logger.debug(f'{time.time() - start_time} seconds - Successfully updated the hist.csv data file '
                 f'for massif: {massif}   ...')

logger.info(f'{time.time() - start_time} seconds - Job succeeded  ...')
